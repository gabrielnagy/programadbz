﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO produtodto)
        {
       
                if (produtodto.Nome_Produto == string.Empty)
                {
                    throw new ArgumentException("Nome do produto é obrigatório.");
                }
                if (produtodto.Valor_Produto == decimal.Zero)
                {
                    throw new ArgumentException("Valor do produto é obrigatório.");
                }
           
            ProdutoDatabase produtodbb = new ProdutoDatabase();
            int id = produtodbb.Salvar(produtodto);
            return id;
        }

        public List<ProdutoDTO> Lista(string Nome_Produto)
        {
            ProdutoDatabase listaBD = new ProdutoDatabase();
            List<ProdutoDTO> listar = listaBD.Consultar(Nome_Produto);

            return listar;  
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }


    }
}
