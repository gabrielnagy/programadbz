﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO produto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, vl_preco) VALUES (@nm_produto,@vl_preco)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto.Nome_Produto));
            parms.Add(new MySqlParameter("vl_preco", produto.Valor_Produto));

            Database db = new Database();
            int id = db.ExecuteInsertScriptWithPK(script, parms);
            return id;
        }

        public List<ProdutoDTO> Consultar(string Nome_Produto)
        {
            string script = @"select * from tb_produto where nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto",Nome_Produto));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome_Produto = reader.GetString("nm_produto");
                dto.Valor_Produto = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }
        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome_Produto = reader.GetString("nm_produto");
                dto.Valor_Produto = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
