﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.PedidoClasses
{
    public class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produto, id_pedido) VALUES (@id_produto, id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IDProduto));
            parms.Add(new MySqlParameter("id_pedito", dto.IDPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }



    }
}
