﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    public class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERTE INTO tb_tabela(nm_cliente, ds_cpf, dt_venda) VALUES (@nm_cliente, @ds_cpf, @dt_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));
        
            Database db = new Database();
            return  db.ExecuteInsertScriptWithPK(script, parms);
        }
    }
}
