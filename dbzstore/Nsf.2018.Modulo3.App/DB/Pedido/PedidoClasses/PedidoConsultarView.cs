﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.PedidoClasses
{
    public class PedidoConsultarView
    {
        public int Id_Pedido { get; set; }

        public string Cliente { get; set; }

        public DateTime Data_Venda { get; set; }

        public decimal Quantidade_Itens { get; set; }

        public decimal Valor_Total { get; set; }
    }
}
