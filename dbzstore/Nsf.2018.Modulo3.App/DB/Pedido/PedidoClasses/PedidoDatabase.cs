﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using Nsf._2018.Modulo3.App.DB.Pedido.PedidoClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    public class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERTE INTO tb_tabela(nm_cliente, ds_cpf, dt_venda) VALUES (@nm_cliente, @ds_cpf, @dt_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));
        
            Database db = new Database();
            return  db.ExecuteInsertScriptWithPK(script, parms);
        }

        public List<PedidoConsultarView> Consultar()
        {
            string script = @"select * from DBZStoreDB.vw_pedido_consultar";
            Database db = new Database();

            List<MySqlParameter> par = new List<MySqlParameter>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, par);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();

            while (reader.Read())
            {
                PedidoConsultarView view = new PedidoConsultarView();
                view.Id_Pedido = reader.GetInt32("id_pedido");
                view.Cliente = reader.GetString("nm_cliente");
                view.Data_Venda = reader.GetDateTime("dt_venda");
                view.Quantidade_Itens = reader.GetDecimal("qtd_itens");
                view.Valor_Total = reader.GetDecimal("vl_total");

                lista.Add(view);


            }
            reader.Close();

            return lista;

        }
    }
}
