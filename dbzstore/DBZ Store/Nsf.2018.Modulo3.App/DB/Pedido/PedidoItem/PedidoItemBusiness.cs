﻿using Nsf._2018.Modulo3.App.DB.Pedido.PedidoClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    public class PedidoItemBusiness
    {
        public int Salvar(PedidoItemDTO dto)
        {
            PedidoItemDatabase database = new PedidoItemDatabase();
            return database.Salvar(dto);
        }
    }
}
