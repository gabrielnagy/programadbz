﻿using Nsf._2018.Modulo3.App.DB.Pedido.PedidoClasses;
using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    public class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();

            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDTO = new PedidoItemDTO();
                itemDTO.IDPedido = idPedido;
                itemDTO.IDProduto = item.ID;

                itemBusiness.Salvar(itemDTO);
            }

            return idPedido;
        }

        public List<PedidoConsultarView> Consultar(string Nome_Cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(Nome_Cliente);
        }


    }
}

