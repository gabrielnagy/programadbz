﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using Nsf._2018.Modulo3.App.DB.Pedido.PedidoClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    public class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido(nm_cliente, ds_cpf, dt_venda) VALUES (@nm_cliente, @ds_cpf, @dt_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));
        
            Database db = new Database();
            return  db.ExecuteInsertScriptWithPK(script, parms);
        }

        public List<PedidoConsultarView> Consultar(string Nome_Cliente)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", Nome_Cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();
            while (reader.Read())
            {
                PedidoConsultarView dto = new PedidoConsultarView();
                dto.Id_Pedido = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.Quantidade_Itens = reader.GetInt32("qtd_itens");
                dto.Data_Venda = reader.GetDateTime("dt_venda");
                dto.Valor_Total = reader.GetDecimal("vl_total");

                //aa Teste
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        
    }
}
