﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    public class ProdutoDTO
    {
        public int ID { get; set; }

        public string Nome_Produto { get; set; }

        public decimal Valor_Produto { get; set; }
    }
}
