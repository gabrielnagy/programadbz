﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPreco.Text == string.Empty)
                {
                    txtPreco.Text = "0";
                }

                ProdutoDTO DTO = new ProdutoDTO();
                DTO.Valor_Produto = Convert.ToDecimal(txtPreco.Text);
                DTO.Nome_Produto = txtProduto.Text;

                ProdutoBusiness buss = new ProdutoBusiness();
                buss.Salvar(DTO);

                txtPreco.Text = string.Empty;
                txtProduto.Text = string.Empty;

                MessageBox.Show("Produto Salvo com Sucesso");

                this.Hide();
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }

        }

    }
}
